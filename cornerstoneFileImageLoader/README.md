cornerstone File Image Loader
=============================

A [cornerstone](https://github.com/cornerstonejs/cornerstone) Image Loader for images (JPG, PNG) using the HTML5 File API or from ArrayBuffer.  

Using the File API presents in HTML5 or ArrayBuffer data is possible open local image such as JPEG and PNG in Cornerstone library.

It's also possible to build a new image from a previously loaded Cornerstone image with a custom pixel data.
使用HTML5中的文件API或ArrayBuffer数据可以在Cornerstone库中打开本地图像，如JPEG和PNG。

也可以从先前加载的Cornerstone图像中用自定义的像素数据建立一个新的图像。

Live Examples
---------------

View the [Universal Dicom Viewer](https://webnamics.github.io/u-dicom-viewer/) built on cornerstone.

Install
-------

Via [NPM](https://www.npmjs.com/):

> npm install cornerstone-file-image-loader


Usage
-----

Simply include the cornerstoneFileImageLoader.js in your HTML file after you load cornerstone.js and then set the cornerstone instance as an external module for cornerstoneFileImageLoader:
只需在加载 cornerstone.js 后，在你的 HTML 文件中包含 cornerstoneFileImageLoader.js，
然后将 cornerstone 实例设置为 cornerstoneFileImageLoader 的外部模块。
````javascript
cornerstoneFileImageLoader.external.cornerstone = cornerstone;
````

This will let cornerstoneFileImageLoader register itself with cornerstone to load imageId's that have the imagefile schemes. 

To display an image, first add a HTML5 file object to cornerstoneFileImageLoader then pass the image as the imageId parameter to a cornerstone loadImage():
这将让 cornerstoneFileImageLoader 向 cornerstone 注册自己，以加载具有 imagefile 方案的 imageId。

要显示图像，首先将一个HTML5文件对象添加到cornerstoneFileImageLoader，然后将图像作为imageId参数传递给cornerstone loadImage()。
````javascript
const imageId = cornerstoneFileImageLoader.fileManager.add(file);
cornerstone.loadImage(imageId).then(function(image) {
	cornerstone.displayImage(element, image);
	...
}
````

Or if previously the JPG or PNG data has loaded in a ArrayBuffer:
或者如果之前的JPG或PNG数据已经加载到ArrayBuffer中。

````javascript
const imageId = cornerstoneFileImageLoader.fileManager.addBuffer(data);
cornerstone.loadImage(imageId).then(function(image) {
	cornerstone.displayImage(element, image);
	...
}
````

To build a new image from loaded Cornerstone Image and a custom pixel data:
从加载的基石图像和自定义像素数据建立一个新的图像。

````javascript
const customObj = {
	rows: rows,				// rows in PixelData
	columns: columns, 		// columns in pixelData
	pixelData: pixelData,	// custom pixel data
	image: loadedImage,		// loaded Cornerstone Image
}
const imageId = cornerstoneFileImageLoader.fileManager.addCustom(customObj);
cornerstone.loadImage(imageId).then(function(image) {
	cornerstone.displayImage(element, image);
	...
}
````

Build System
============

This project uses webpack to build the software.

Pre-requisites:
---------------

NodeJs - [click to visit web site for installation instructions](http://nodejs.org).

Common Tasks
------------

Update dependencies (after each pull):
> npm install

Running the build:
> npm start

Automatically running the build and unit tests after each source change:
在每次源码变更后，自动运行构建和单元测试。
> npm run watch

Why is this a separate library from cornerstone?
================================================

Cornerstone was designed to support loading of any kind of image regardless of its container,
compression algorithm, encoding or transport.  This is one of many possible image loaders
that can provide the image pixel data to cornerstone to display
Cornerstone被设计为支持加载任何类型的图像，无论其容器如何。
压缩算法、编码或传输。 这是许多可能的图像加载器中的一个
中的一种，它可以向基石提供图像像素数据以显示

Copyright
=========
Copyright 2019 Luigi Orso [webnamics@gmail.com](mailto:webnamics@gmail.com)

